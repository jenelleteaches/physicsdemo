//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    
    override func didMove(to view: SKView) {
        self.backgroundColor = UIColor.white
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        
        
        circle.position = CGPoint(x:self.size.width/2, y:self.size.height-50)
        square.position = CGPoint(x:self.size.width*0.25, y:self.size.height/2)
        triangle.position = CGPoint(x:self.size.width*0.75, y:self.size.height/2)
        
        
        // draw hitboxes
        
        // 1. create the hitbox
        self.circle.physicsBody = SKPhysicsBody(rectangleOf: self.circle.frame.size)
        
        self.square.physicsBody = SKPhysicsBody(rectangleOf: self.square.frame.size)
        
        self.triangle.physicsBody = SKPhysicsBody(rectangleOf: self.triangle.frame.size)
        
        
        
        
        addChild(circle)
        addChild(square)
        addChild(triangle)
        
        spawnSand()
    }
    
    func spawnSand() {
        let sand = SKSpriteNode(imageNamed: "sand")
        
        let x = self.size.width/2
        let y = self.size.height - 100
        sand.position.x = x
        sand.position.y = y
        
        
        sand.physicsBody = SKPhysicsBody(rectangleOf: sand.frame.size)
        sand.physicsBody?.affectedByGravity = true
        
        addChild(sand)
    }
    
    override func update(_ currentTime: TimeInterval) {
        self.spawnSand()
    }
    
}
